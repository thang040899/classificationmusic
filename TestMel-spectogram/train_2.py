from __future__ import print_function
from __future__ import absolute_import

from keras import backend as K
from keras.layers import Input, Dense
from keras.models import Model
from keras.layers import Dense, Dropout, Flatten
from keras.layers.convolutional import Convolution2D
from keras.layers.convolutional import MaxPooling2D, ZeroPadding2D
from keras.layers.normalization import BatchNormalization
from keras.layers.advanced_activations import ELU
from keras.utils.data_utils import get_file
from keras.layers import Input, Dense

import json
import numpy as np
from sklearn.model_selection import train_test_split
import tensorflow.keras as keras
import matplotlib.pyplot as plt
import os

from tensorflow.python.keras.saving.save import load_model

INDEX = 0
INPUT_MFCC = r"/content/gdrive/MyDrive/KLTN/DatasetMFCCs_120k"
OUTPUT_MODEL = r"/content/gdrive/MyDrive/KLTN/Model_120k/model_40k_new_"+str(INDEX)+".h5"
BEFORE_MODEL = r"/content/gdrive/MyDrive/KLTN/Model_120k/model_40k_new_"+str(INDEX-1)+".h5"

mapppingVN =[
    "viet-nam-cai-luong-",
    "viet-nam-nhac-tre-v-pop-",
    "viet-nam-nhac-trinh-",
    "viet-nam-nhac-tru-tinh-",
    "viet-nam-rap-viet-",
    "viet-nam-nhac-thieu-nhi-",
    "viet-nam-nhac-cach-mang-",
    "viet-nam-nhac-dan-ca-que-huong-",
    # "viet-nam-nhac-khong-loi-",
]

mappingAU = [
  "au-my-classical-",
  "au-my-folk-",
  "au-my-country-",
#   "au-my-pop-",
  "au-my-rock-",
#   "au-my-latin-",
  "au-my-rap-hip-hop-",
  "au-my-alternative-",
#   "au-my-blues-jazz-",
#   "au-my-reggae-",
#   "au-my-r-b-soul-",
]

mappingFull = mapppingVN+mappingAU

def load_data():
    """Loads training dataset from json file.
        :param data_path (str): Path to json file containing data
        :return X (ndarray): Inputs
        :return y (ndarray): Targets
    """
    data ={
        "mfcc":[],
        "labels":[]
    }
    label = 0
    for fol in os.listdir(INPUT_MFCC):
        if fol in mappingAU :
            pathFol = os.path.join(INPUT_MFCC, fol)
            for filename in os.listdir(os.path.join(INPUT_MFCC, fol)):
                if filename[-7:] == "-"+str(INDEX)+".json":
                    fullPath = os.path.join(pathFol, filename)
                    print(fullPath)
                    with open(fullPath, "r") as fp:
                        mfcc_json = json.load(fp)
                    data["mfcc"] += mfcc_json["mfcc"]
                    data["labels"] += [label]*len(mfcc_json["mfcc"])
                    fp.close()
            label += 1


    # with open(data_path, "r") as fp:
    #     data = json.load(fp)
    print("Len mfcc: "+str(len(data["mfcc"])))
    print("Len labels: "+str(len(data["labels"])))

    X = np.array(data["mfcc"])
    y = np.array(data["labels"])
    return X, y


def plot_history(history):
    """Plots accuracy/loss for training/validation set as a function of the epochs
        :param history: Training history of model
        :return:
    """

    fig, axs = plt.subplots(2)

    # create accuracy sublpot
    axs[0].plot(history.history["accuracy"], label="train accuracy")
    axs[0].plot(history.history["val_accuracy"], label="test accuracy")
    axs[0].set_ylabel("Accuracy")
    axs[0].legend(loc="lower right")
    axs[0].set_title("Accuracy eval")

    # create error sublpot
    axs[1].plot(history.history["loss"], label="train error")
    axs[1].plot(history.history["val_loss"], label="test error")
    axs[1].set_ylabel("Error")
    axs[1].set_xlabel("Epoch")
    axs[1].legend(loc="upper right")
    axs[1].set_title("Error eval")

    plt.show()


def prepare_datasets(test_size, validation_size):
    """Loads data and splits it into train, validation and test sets.
    :param test_size (float): Value in [0, 1] indicating percentage of data set to allocate to test split
    :param validation_size (float): Value in [0, 1] indicating percentage of train set to allocate to validation split
    :return X_train (ndarray): Input training set
    :return X_validation (ndarray): Input validation set
    :return X_test (ndarray): Input test set
    :return y_train (ndarray): Target training set
    :return y_validation (ndarray): Target validation set
    :return y_test (ndarray): Target test set
    """

    # load data
    X, y = load_data()

    # create train, validation and test split
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size)
    X_train, X_validation, y_train, y_validation = train_test_split(X_train, y_train, test_size=validation_size)

    # add an axis to input sets
    X_train = X_train[..., np.newaxis]
    X_validation = X_validation[..., np.newaxis]
    X_test = X_test[..., np.newaxis]

    return X_train, X_validation, X_test, y_train, y_validation, y_test


def build_model(input_shape,weights='msd', input_tensor=None,
                   include_top=True):
    # if weights not in {'msd', None}:
    #     raise ValueError('The `weights` argument should be either '
    #                      '`None` (random initialization) or `msd` '
    #                      '(pre-training on Million Song Dataset).')

    # Determine proper input shape
    # if K.image_dim_ordering() == 'th':
    #     input_shape = (1, 96, 1366)
    # else:
    #     input_shape = (96, 1366, 1)

    if input_tensor is None:
        melgram_input = Input(shape=input_shape)
    else:
        if not K.is_keras_tensor(input_tensor):
            melgram_input = Input(tensor=input_tensor, shape=input_shape)
        else:
            melgram_input = input_tensor

    # Determine input axis
    channel_axis = 1
    freq_axis = 2
    time_axis = 3
    # if K.image_dim_ordering() == 'th':
    #     channel_axis = 1
    #     freq_axis = 2
    #     time_axis = 3
    # else:
    #     channel_axis = 3
    #     freq_axis = 1
    #     time_axis = 2

    # Input block
    x = BatchNormalization(axis=freq_axis, name='bn_0_freq')(melgram_input)

    # Conv block 1
    x = Convolution2D(64, 3, 3, padding='same', name='conv1')(x)
    x = BatchNormalization(axis=channel_axis,  name='bn1')(x)
    x = ELU()(x)
    x = MaxPooling2D((3, 3), strides=(2, 2), padding='same')(x)

    # Conv block 2
    x = Convolution2D(128, 3, 3, padding='same', name='conv2')(x)
    x = BatchNormalization(axis=channel_axis,  name='bn2')(x)
    x = ELU()(x)
    x = MaxPooling2D((3, 3), strides=(2, 2), padding='same')(x)

    # Conv block 3
    x = Convolution2D(128, 3, 3, padding='same', name='conv3')(x)
    x = BatchNormalization(axis=channel_axis,  name='bn3')(x)
    x = ELU()(x)
    x = MaxPooling2D((3, 3), strides=(2, 2), padding='same')(x)

    # Conv block 4
    x = Convolution2D(128, 3, 3, padding='same', name='conv4')(x)
    x = BatchNormalization(axis=channel_axis,  name='bn4')(x)
    x = ELU()(x)
    x = MaxPooling2D((3, 3), strides=(2, 2), padding='same')(x)

    # Conv block 5
    x = Convolution2D(64, 3, 3, padding='same', name='conv5')(x)
    x = BatchNormalization(axis=channel_axis,  name='bn5')(x)
    x = ELU()(x)
    x = MaxPooling2D((3, 3), strides=(2, 2), padding='same')(x)

    # Output
    x = Flatten()(x)
    if include_top:
        x = Dense(2, activation='sigmoid', name='output')(x)

    # Create model
    model = Model(melgram_input, x)
    return model
    # if weights is None:
    #     return model    
    # else: 
    #     # Load input
    #     if K.image_dim_ordering() == 'tf':
    #         raise RuntimeError("Please set image_dim_ordering == 'th'."
    #                            "You can set it at ~/.keras/keras.json")
    #     model.load_weights('data/music_tagger_cnn_weights_%s.h5' % K._BACKEND,
    #                        by_name=True)
    #     return model


def predict(model, X, y):
    """Predict a single sample using the trained model
    :param model: Trained classifier
    :param X: Input data
    :param y (int): Target
    """

    # add a dimension to input data for sample - model.predict() expects a 4d array in this case
    X = X[np.newaxis, ...] # array shape (1, 130, 13, 1)

    # perform prediction
    prediction = model.predict(X)

    # get index with max value
    predicted_index = np.argmax(prediction, axis=1)

    print("Target: {}, Predicted label: {}".format(y, predicted_index))

def train():
    # get train, validation, test splits
    X_train, X_validation, X_test, y_train, y_validation, y_test = prepare_datasets(0.25, 0.2)

    # # create network
    input_shape = (X_train.shape[1], X_train.shape[2], 1)
    if INDEX == 0:
        model = build_model(input_shape)
    else:
        model = load_model(BEFORE_MODEL)

    # # compile model
    optimiser = keras.optimizers.Adam(learning_rate=0.0001)
    model.compile(optimizer=optimiser,
                  loss='sparse_categorical_crossentropy',
                  metrics=['accuracy'])

    model.summary()

    # # train model
    history = model.fit(X_train, y_train, validation_data=(X_validation, y_validation), batch_size=32, epochs=30)

    # plot accuracy/error for training and validation
    plot_history(history)
    model.save(OUTPUT_MODEL)
    # # evaluate model on test set
    test_loss, test_acc = model.evaluate(X_test, y_test, verbose=2)
    print('\nTest accuracy:', test_acc)

    # pick a sample to predict from the test set
    X_to_predict = X_test[100]
    y_to_predict = y_test[100]

    #print(X_to_predict)
    # predict sample
    predict(model, X_to_predict, y_to_predict)

if __name__ == "__main__":

    model = build_model((130,13,1))

    # # compile model
    optimiser = keras.optimizers.Adam(learning_rate=0.0001)
    model.compile(optimizer=optimiser,
                  loss='sparse_categorical_crossentropy',
                  metrics=['accuracy'])

    model.summary()
    # while INDEX < 8:
    #     train()
    #     INDEX += 1

